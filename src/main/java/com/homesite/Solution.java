package com.homesite;

import com.homesite.controltable.Evaluator;
import com.homesite.controltable.impl.DefaultEvaluator;
import com.homesite.controltable.model.ControlTableRow;
import org.junit.jupiter.api.Assertions;

import java.util.Arrays;
import java.util.HashMap;

public class Solution {

    public static void main(String[] args) {

        Evaluator evaluator = new DefaultEvaluator();

        boolean result1 = evaluator.evaluate(
                new HashMap<>() {{
                    put("RoofAge", "12");
                    put("YearBuilt", "1995");
                }},
                Arrays.asList(
                    new ControlTableRow("RoofAge", "=", "12"),
                    new ControlTableRow("YearBuilt", "RANGE", "1990-2000")
                )
        );

        System.out.println("result1=" + result1);

        boolean result2 = evaluator.evaluate(
                new HashMap<>() {{
                    put("RoofAge", "12");
                    put("YearBuilt", "2009");
                }},
                Arrays.asList(
                        new ControlTableRow("RoofAge", "=", "12"),
                        new ControlTableRow("YearBuilt", "RANGE", "1990-2000")
                )
        );

        System.out.println("result2=" + result2);

    }

}
