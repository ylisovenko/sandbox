package com.homesite.controltable;

import com.homesite.controltable.expression.*;
import com.homesite.controltable.model.ControlTableRow;
import com.homesite.controltable.model.Operator;

public class ExpressionFactory {

    public static Expression createExpression(ControlTableRow controlTableRow) {

        Operator op = Operator.fromStringRepresentation(controlTableRow.getOperator());

        Expression expression = null;
        try {
            expression = switch (op) {
                case EQUAL -> new EqualExpression(controlTableRow.getField(), controlTableRow.getValue());
                case NOT_EQUAL -> new NotEqualExpression(controlTableRow.getField(), controlTableRow.getValue());
                case GREATER -> new GreaterExpression(controlTableRow.getField(), controlTableRow.getValue());
                case GREATER_OR_EQUAL -> new GreaterOrEqualExpression(controlTableRow.getField(), controlTableRow.getValue());
                case LESS -> new LessExpression(controlTableRow.getField(), controlTableRow.getValue());
                case LESS_OR_EQUAL -> new LessOrEqualExpression(controlTableRow.getField(), controlTableRow.getValue());
                case IN_RANGE -> new RangeExpression(controlTableRow.getField(), controlTableRow.getValue());
                case ANY_OF -> new AnyOfExpression(controlTableRow.getField(), controlTableRow.getValue());
                default -> throw new UnsupportedOperationException(String.format("Operator %s is not supported", op.getStringRepresentation()));
            };
        }
        catch (Exception ex) {
            // TODO throw custom exception
        }

        return expression;
    }

}
