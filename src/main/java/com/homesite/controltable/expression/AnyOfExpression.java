package com.homesite.controltable.expression;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AnyOfExpression extends Expression {

    private final Set<String> allowedValues;

    public Set<String> getAllowedValues() {
        return allowedValues;
    }

    public AnyOfExpression(String field, String value) {
        super(field, value);
        allowedValues = new HashSet<>(Arrays.asList(value.split(",")));
    }

    @Override
    public boolean evaluate(Map<String, String> data) {

        String actualValue = data.get(field);

        boolean result = allowedValues.contains(actualValue);

        if (!result) {
            System.out.println(String.format("AnyOfExpression failed: %s is missing in the list of allowed values %s", actualValue, allowedValues));  // TODO replace with log
        }

        return result;
    }
}
