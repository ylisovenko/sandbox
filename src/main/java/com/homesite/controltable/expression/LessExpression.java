package com.homesite.controltable.expression;

import org.apache.commons.lang.StringUtils;

import java.util.Map;

public class LessExpression extends Expression {

    private final Integer max;

    public Integer getMax() {
        return max;
    }

    public LessExpression(String field, String value) {
        super(field, value);
        max = Integer.valueOf(value);
    }

    @Override
    public boolean evaluate(Map<String, String> data) {

        String actualValueStr = data.get(field);
        if (StringUtils.isNotBlank(actualValueStr)) {
            Integer actualValue = Integer.valueOf(actualValueStr);

            boolean result = actualValue < max;

            if (!result) {
                System.out.println(String.format("LessExpression failed: %s is not less than %s", actualValue, max));  // TODO replace with log
            }

            return result;
        }
        return false;
    }
}
