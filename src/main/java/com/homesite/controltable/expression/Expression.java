package com.homesite.controltable.expression;

import com.homesite.controltable.model.Operator;

import java.util.Map;

public abstract class Expression {

    protected final String field;
    protected final String value;

    public String getField() {
        return field;
    }

    public String getValue() {
        return value;
    }

    protected Expression(String field, String value) {
        this.field = field;
        this.value = value;
    }

    public abstract boolean evaluate(Map<String, String> data);

}
