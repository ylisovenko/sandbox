package com.homesite.controltable.expression;

import org.apache.commons.lang.StringUtils;

import java.util.Map;

public class GreaterOrEqualExpression extends Expression {

    private final Integer min;

    public Integer getMin() {
        return min;
    }

    public GreaterOrEqualExpression(String field, String value) {
        super(field, value);
        min = Integer.valueOf(value);
    }

    @Override
    public boolean evaluate(Map<String, String> data) {

        String actualValueStr = data.get(field);
        if (StringUtils.isNotBlank(actualValueStr)) {
            Integer actualValue = Integer.valueOf(actualValueStr);

            boolean result = actualValue >= min;

            if (!result) {
                System.out.println(String.format("GreaterOrEqualExpression failed: %s is not greater or equal to %s", actualValue, min));  // TODO replace with log
            }

            return result;
        }
        return false;
    }
}
