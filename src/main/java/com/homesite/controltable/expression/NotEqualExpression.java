package com.homesite.controltable.expression;

import java.util.Map;

public class NotEqualExpression extends Expression {

    public NotEqualExpression(String field, String value) {
        super(field, value);
    }

    @Override
    public boolean evaluate(Map<String, String> data) {
        String actualValue = data.get(field);

        boolean result = actualValue == null || !actualValue.equals(value);

        if (!result) {
            System.out.println(String.format("NotEqualExpression failed: %s is not defined or equal to %s", actualValue, value));  // TODO replace with log
        }

        return result;
    }

}
