package com.homesite.controltable.expression;

import java.util.Map;

public class EqualExpression extends Expression {

    public EqualExpression(String field, String value) {
        super(field, value);
    }

    @Override
    public boolean evaluate(Map<String, String> data) {
        String actualValue = data.get(field);

        boolean result = actualValue != null && actualValue.equals(value);

        if (!result) {
            System.out.println(String.format("EqualExpression failed: %s is not defined or not equal to %s", actualValue, value));  // TODO replace with log
        }

        return result;
    }

}
