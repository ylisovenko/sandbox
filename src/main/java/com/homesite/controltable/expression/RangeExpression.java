package com.homesite.controltable.expression;

import org.apache.commons.lang.StringUtils;

import java.util.Map;

public class RangeExpression extends Expression {

    private final Integer min;
    private final Integer max;

    public Integer getMin() {
        return min;
    }

    public Integer getMax() {
        return max;
    }

    public RangeExpression(String field, String value) {
        super(field, value);
        String[] range = value.split("-");
        min = Integer.valueOf(range[0]);
        max = Integer.valueOf(range[1]);
    }

    @Override
    public boolean evaluate(Map<String, String> data) {

        String actualValueStr = data.get(field);
        if (StringUtils.isNotBlank(actualValueStr)) {
            Integer actualValue = Integer.valueOf(actualValueStr);

            boolean result = actualValue >= min && actualValue <= max;

            if (!result) {
                System.out.println(String.format("RangeExpression failed: %s is not in allowed range [%s, %s]", actualValue, min, max));  // TODO replace with log
            }

            return result;
        }
        return false;
    }
}
