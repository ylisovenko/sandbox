package com.homesite.controltable.impl;

import com.homesite.controltable.Evaluator;
import com.homesite.controltable.ExpressionFactory;
import com.homesite.controltable.model.ControlTableRow;

import java.util.List;
import java.util.Map;

public class DefaultEvaluator implements Evaluator {

    public boolean evaluate(Map<String, String> data, List<ControlTableRow> controlTableRows) {

        boolean result = controlTableRows.stream()
                .allMatch(controlTableRow -> ExpressionFactory.createExpression(controlTableRow).evaluate(data));

        // TODO log result

        return result;
    }

}
