package com.homesite.controltable;

import com.homesite.controltable.model.ControlTableRow;

import java.util.List;
import java.util.Map;

public interface Evaluator {

    boolean evaluate(Map<String, String> data, List<ControlTableRow> controlTableRows);

}
