package com.homesite.controltable.model;

public class ControlTableRow {

    private final String field;
    private final String operator;
    private final String value;

    public String getField() {
        return field;
    }

    public String getOperator() {
        return operator;
    }

    public String getValue() {
        return value;
    }

    public ControlTableRow(String field, String operator, String value) {
        this.field = field;
        this.operator = operator;
        this.value = value;
    }

}
