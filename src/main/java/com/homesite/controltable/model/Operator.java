package com.homesite.controltable.model;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum Operator {

    EQUAL("="),
    NOT_EQUAL("!="),
    GREATER(">"),
    GREATER_OR_EQUAL(">="),
    LESS("<"),
    LESS_OR_EQUAL("<="),
    IN_RANGE("RANGE"),
    ANY_OF("ANY");

    private static final Map<String, Operator> ENUM_MAP;

    static {
        Map<String, Operator> map = new ConcurrentHashMap<>();
        for (Operator instance : Operator.values()) {
            map.put(instance.getStringRepresentation().toLowerCase(),instance);
        }
        ENUM_MAP = Collections.unmodifiableMap(map);
    }

    private final String stringRepresentation;

    public String getStringRepresentation() {
        return  stringRepresentation;
    }

    Operator(String stringRepresentation) {
        this.stringRepresentation = stringRepresentation;
    }

    public static Operator fromStringRepresentation(String stringRepresentation) {

       Operator op = ENUM_MAP.get(stringRepresentation.toLowerCase());

       if (op == null) {
           throw new IllegalArgumentException(String.format("Unable to convert %s to Operator enumeration", stringRepresentation));
       }

       return op;
    }

}
