package com.homesite.controltable;

import com.homesite.controltable.impl.DefaultEvaluator;
import com.homesite.controltable.model.ControlTableRow;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EvaluatorTest {

    @Test
    public void testDefaultEvaluator_test1() {

        Evaluator evaluator = new DefaultEvaluator();

        boolean result = evaluator.evaluate(
                new HashMap<>() {{
                    put("RoofType", "Asphalt Shingles");
                    put("RoofAge", "12");
                    put("YearBuilt", "2009");
                }},
                Arrays.asList(
                        new ControlTableRow("RoofAge", "RANGE", "10-15"),
                        new ControlTableRow("YearBuilt", ">", "2000"),
                        new ControlTableRow("RoofType", "ANY", "Clay Tile,Asphalt Shingles")
                )
        );

        assertTrue(result);
    }

    @Test
    public void testDefaultEvaluator_test2() {

        Evaluator evaluator = new DefaultEvaluator();

        boolean result = evaluator.evaluate(
                new HashMap<>() {{
                    put("RoofType", "Foam");
                    put("RoofAge", "12");
                    put("YearBuilt", "2009");
                }},
                Arrays.asList(
                        new ControlTableRow("RoofAge", "=", "12"),
                        new ControlTableRow("YearBuilt", "RANGE", "1990-2000")
                )
        );

        assertFalse(result);
    }

}
